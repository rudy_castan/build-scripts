wd=$PWD

# Xcode project's target name
TARGET=$1
# Path to the xcodeproject file
PROJECT=$2
# Config like Debug Opt Dev
CONFIG=$3
# Path to a provisioning profile to build the app with
PROFILE=$4
#path to and name of ipa file ie, build/ipa/awesome_app
# it will append ipa
# need to specify a full path to PackageApplication
# we are assuming $5 is relative to the workspace's relative directory
IPA_DEST="${wd}/$5"

# defines PROVISIONING_PROFILE_UUID
. scripts/ios/install_prov_profile.sh "${PROFILE}"

echo '###########################################################################'
echo '#' Build Project
echo '###########################################################################'


# delete the 'build' folder made in same directory as the project file
build_dir_path=${PROJECT%/*}/build
[ -d ${build_dir_path} ] && rm -rf ${build_dir_path}

# build
xcodebuild -target "${TARGET}" -project "${PROJECT}" -configuration "${CONFIG}" clean build CONFIGURATION_BUILD_DIR="${wd}/ios/build" CONFIGURATION_TEMP_DIR="${wd}/ios/build_tmp" PROVISIONING_PROFILE=${PROVISIONING_PROFILE_UUID}

echo '###########################################################################'
echo '#' Make ipa
echo '###########################################################################'
# Locate the generate *.app folder
app=$(FIND ${wd}/ios/build -name *.app)
# delete the destination folder for the ipa
[ -d ${IPA_DEST%/*} ] && rm -rf ${IPA_DEST%/*}
# make the destination folder
mkdir -p ${IPA_DEST%/*}
# turn the app into an ipa
xcrun -sdk iphoneos PackageApplication ${app} -o "${IPA_DEST}.ipa"

dsym=$(FIND ${wd}/ios/build -name *.dSYM)

zip -r "${IPA_DEST}-dSYM.zip" "${dsym}"