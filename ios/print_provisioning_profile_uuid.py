#!/usr/bin/env python
import optparse
import sys

# Prints the Universally Unique Identifier (UUID) for a provisioning profile.
# Exits the script on failure.
def printUUID(ProfilePath):

	# Provisioning profiles appear to be some variant of the plist format, but
	# I can't convert them to xml using plutil or load them using plistlib.
	# As a workaround, we just open it as a text file and doing some string 
	# manipulation to find the UUID value.
	try:
		file = open(ProfilePath, "r")
	except IOError: 
		print "Error: Profile not found: " + ProfilePath
		sys.exit(1)
	
	uuid = None
	found_uuid = False
	for line in file.readlines():
		if line.find("<key>UUID</key>") != -1:
			found_uuid = True
			continue
		if line.find("<string>") != -1 and found_uuid:
			uuid = line.strip()
			uuid = uuid.replace("<string>", "")
			uuid = uuid.replace("</string>", "")
			break
		
	if uuid is None:
		print "Error: Couldn't find UUID in profile: " + ProfilePath
		sys.exit(1)
	else:
		print uuid
						
def main():
	p = optparse.OptionParser(description='Prints the UUID of a given provisioning profile file (.mobileprovision) to standard out.',
							  prog='get_provisioning_profile_uuid',
							  version='1.1',
							  usage='%prog ProvisioningProfilePath')

	options, arguments = p.parse_args()
	
	if len(arguments) == 1:
		printUUID(arguments[0])
	else:
		p.print_help()
		sys.exit(1)

if __name__ == '__main__':
	main()

