# Xcode project's target name
TARGET=$1
# Path to the xcodeproject file
PROJECT=$2
# Config like Debug Opt Dev
CONFIG=$3

echo '###########################################################################'
echo '#' Build Project
echo '###########################################################################'

# delete the 'build' folder made in same directory as the project file
build_dir_path=${PROJECT%/*}/build
[ -d ${build_dir_path} ] && rm -rf ${build_dir_path}

wd=$PWD

# build
xcodebuild -target "${TARGET}" -project "${PROJECT}" -configuration "${CONFIG}" clean build CONFIGURATION_BUILD_DIR="${wd}/ios/build" CONFIGURATION_TEMP_DIR="${wd}/ios/build_tmp"
