#!/bin/bash

###########################################################################
# iOS - Install App provisioning profile for Xcode
###########################################################################

chmod 777 scripts/ios/print_provisioning_profile_uuid.py

PROFILE_PATH=$1

PROVISIONING_PROFILE_UUID=`"scripts/ios/print_provisioning_profile_uuid.py" "${PROFILE_PATH}"`
echo PROVISIONING_PROFILE_UUID=${PROVISIONING_PROFILE_UUID}


# Copy/install the provision to a place where Xcode will find it
if [ ! -d "${HOME}/Library/MobileDevice/Provisioning Profiles" ]; then
     mkdir -p "${HOME}/Library/MobileDevice/Provisioning Profiles"
fi

cp "${PROFILE_PATH}" "${HOME}/Library/MobileDevice/Provisioning Profiles/${PROVISIONING_PROFILE_UUID}.mobileprovision"
