import re

def parse_file(profile_file_path):
	key = None
	f = open(profile_file_path, 'rU')
	prop_map = {}
	for line in f:
		if line.find('</key>') != -1:
			key = re.sub(r'\<.*?\>', '', line.strip())
		elif key:
			prop_map[key] = re.sub(r'\<.*?\>', '', line.strip())
			key = None
	f.close()
	return prop_map

class FileInfo:
	"""A simple class to get data from an iOS mobileprovision file"""
	def __init__(self, profile_file_path):		
		properties = parse_file(profile_file_path)
		self.AppIDName 				= properties['AppIDName']
		self.CreationDate 			= properties['CreationDate']
		self.ApplicationIdentifier 	= properties['application-identifier']
		self.ExpirationDate 		= properties['ExpirationDate']
		self.Name 					= properties['Name']
		self.TeamName 				= properties['TeamName']
		self.UUID 					= properties['UUID']

	def __str__(self):
		return str({ 
			  'AppIDName':self.AppIDName
			, 'CreationDate':self.CreationDate
			, 'application-identifier':self.ApplicationIdentifier
			, 'ExpirationDate':self.ExpirationDate
			, 'Name':self.Name
			, 'TeamName':self.TeamName
			, 'UUID':self.UUID
			})

	def __eq__(self, other):
		if isinstance(other, FileInfo):
			return self.UUID == other.UUID
		return False
	def __ne__(self, other):
		return not self.__eq__(other)